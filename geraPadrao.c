#include<stdio.h>
#include<string.h>
#include<conio.h>
#include<stdlib.h>
#include<locale.h>
#include<time.h>


#define tam_str 200

typedef struct img {
  int lin;
  int col;
  int banda;
  char nome[tam_str];
  // unidade: n�mero de pixel
  int W;  //P1: largura do primeiro ret�ngulo preto
  int H;  //P2: altura do primeiro ret�ngulo preto
  int A;  //P3: largura do primeiro ret�ngulo branco na 1� linha. � tamb�m a largura da 2� coluna
  int B;  //P4: altura da  2� linha
  int Iw; //P5: incremento na largura dos ret�ngulos pretos
  int Ih; //P6: incremento na altura dos ret�ngulos pretos
  int Ia; //P7: incremento na largura dos ret�ngulo brancos
  int Ib; //P8: incremento na altura dos ret�ngulo brancos
  int nh; // qtos blocos pretos na horizontal
  int nv; // qtos blocos pretos na vertical
  int **dado;
} IMG;

FILE * abreArq(char *nome);
void readHeader(IMG *I);
IMG* img_cria();
void geraQuadrado(IMG *I);
void geraColuna(char *nome, int lin, int col, int low, int high );
void geraLinha(char *nome, int lin, int col, int low, int high );
void geraRand(char *nome, int lin, int col, int low, int high );
void geraRandL(char *nome, int lin, int col, int low, int high );
void geraRandC(char *nome, int lin, int col, int low, int high );

FILE *ifpa;
/******************************
 *
 *
 ******************************/
int main(int argc, char *argv[])
{
  //IMG *Im;
  setlocale(LC_ALL,"");
  IMG *I = img_cria();
  printf("argc: %d\n",argc);;
  if(argc == 12)
  {
    strcpy(I->nome,argv[1]);
    I->W = atoi(argv[2]);
    I->H = atoi(argv[3]);
    I->A = atoi(argv[4]);
    I->B = atoi(argv[5]);
    I->Iw = atoi(argv[6]);
    I->Ih = atoi(argv[7]);
    I->Ia = atoi(argv[8]);
    I->Ib = atoi(argv[9]);
    I->nh = atoi(argv[10]);
    I->nv = atoi(argv[11]);

  } else if( argc == 7)
  {
    ifpa = abreArq(argv[1]);
    if(strcmp(argv[2],"-L")==0)
      geraLinha(argv[1], atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), atoi(argv[6]) );
    else if(strcmp(argv[2],"-C")==0)
      geraColuna(argv[1], atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), atoi(argv[6]) );
    else if(strcmp(argv[2],"-RL")==0)
      geraRandL(argv[1], atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), atoi(argv[6]) );
    else if(strcmp(argv[2],"-RC")==0)
      geraRandC(argv[1], atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), atoi(argv[6]) );
    else
      geraRand(argv[1], atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), atoi(argv[6]) );
    fclose(ifpa);
    return 0;
    // void geraLinha(char *nome, int lin, int col, int low, int high )

  } else
  {
    printf("Digite o nome do arquivo.pgm : " );
    scanf("%s", I->nome);

    printf("Digite a largura do primeiro ret�ngulo preto P1(W): ");
    scanf("%d", &I->W);

    printf("Digite a altura do primeiro ret�ngulo preto P2(H): ");
    scanf("%d", &I->H);

    printf("Digite a largura do primeiro ret�ngulo branco na 1� linha. � tamb�m a largura da 2� coluna P3(A): ");
    scanf("%d", &I->A);

    printf("Digite a altura da  2� linha P4(B): ");
    scanf("%d", &I->B);

    printf("Digite o incremento na largura dos ret�ngulos preto  P5(Iw): ");
    scanf("%d", &I->Iw);

    printf("Digite o incremento na altura dos ret�ngulos preto   P6(Ih): ");
    scanf("%d", &I->Ih);

    printf("Digite o incremento na largura dos ret�ngulo branso da  das colunas pares (2�, 4�,2�, 8�)  P7(Ia): ");
    scanf("%d", &I->Ia);

    printf("Digite o incremento na altura das linhas pares (2�, 4�,6�, 8�)  P8(Ib): ");
    scanf("%d", &I->Ib);

    printf("Digite a quantidade de blocos na horizontal: ");
    scanf("%d", &I->nh);

    printf("Digite a quantidade de blocos na vertical: ");
    scanf("%d", &I->nv);
  }

  printf("Starting....\n");

  ifpa = abreArq(I->nome);
  geraQuadrado(I);
  //readHeader(I);
  fclose(ifpa);
  return 0;
}
/**********************************
 *
 *
 **********************************/
void readHeader(IMG *I)
{


}

/***************************
 *
 *
 ***************************/
FILE * abreArq(char *nome)
{
  FILE *f;

  f = fopen(nome,"wb");
  if(f == NULL)
  {
    printf("N�o foi poss�vel abrir o arquivo: %s\n",nome);
    exit(1);
  }
  return f;
}

IMG *img_cria()
{
  IMG *I = (IMG *)malloc(sizeof(IMG));
  I->lin=0;
  I->col=0;
  I->banda=0;
  I->nome[0]='\0';
  // unidade: n�mero de pixel
  I->W=0;  //P1: largura do primeiro ret�ngulo preto
  I->H=0;  //P2: altura do primeiro ret�ngulo preto
  I->A=0;  //P3: largura do primeiro ret�ngulo branco na 1� linha. � tamb�m a largura da 2� coluna
  I->B=0;  //P4: altura da  2� linha
  I->Iw=0; //P5: incremento na largura dos ret�ngulos preto
  I->Ih=0; //P6: incremento na altura dos ret�ngulos preto
  I->Ia=0; //P7: incremento na largura dos ret�ngulo branso da  1� linha
  I->Ib=0; //P8: incremento na altura das linhas pares (2�, 4�, 8�)

  I->dado=NULL;
  return I;
}

/****************************
 *
 *
 ****************************/
void geraQuadrado(IMG *I)
{
  int i, j, lin=0, col=0, paw,pah,paa, pab, pawn,pahn,paan, pabn;
  int ia,ib, iw, ih;
  //char str[3];
  //fprintf(ifpa,"P2\n");
  printf("P5\n");
  fprintf(ifpa,"P5\n");
  // an = termo geral da pa  -- encontrando a largura do enesimo quandrado a ser gerado
  // an = a1 + (n -1)*r
  pawn = I->W + (I->nh-1)*I->Iw;
  paan = I->A + (I->nh-1)*I->Ia;
  pabn = I->B + (I->nv-1)*I->Ib;
  pahn = I->H + (I->nv-1)*I->Ih;

  // somas dos termos de uma pa
  // sn = (a1 + an)*n
  //      -----------
  //          2
  paw = ((pawn + I->W)*I->nh)/2;
  paa = ((paan + I->A)*I->nh)/2;
  pah = ((pahn + I->H)*I->nv)/2;
  pab = ((pabn + I->B)*I->nv)/2;
  //printf("Paw: %d\n",paw);
  //printf("Paa: %d\n",paa);
  col = paw + paa ;
  lin = pah + pab;
  //fprintf(ifpa,"# quandrado gerado pelo geraPadr�o\n");
  printf("# quandrado gerado pelo geraPadr�o\n");
  fprintf(ifpa,"# quandrado gerado pelo geraPadr�o\n");
  //fprintf(ifpa,"%d %d\n", lin, col );
  printf("%d %d\n", col, lin );
  fprintf(ifpa,"%d %d\n", col, lin );
  printf("%d\n", 255);
  fprintf(ifpa,"%d\n", 255);

  // quantidade de quadrados preto | ou quantidade de quadrados branco
  for(lin =0, ih=I->H, ib=I->B ; lin < I->nv ; ib= ib + I->Ib, ih = ih + I->Ih, lin++) // gera as linhas
  {
    for( i=0; i< ih; i++ ) // gerando as linhas do bloco H
    {
      // gerando as colunas  pares
      for( col=0, iw=I->W, ia=I->A; col< I->nh; col++, iw = iw + I->Iw, ia = ia + I->Ia )
      {
        // geras as colunas do bloco w   |    w + Iw
        for( j=0; j < iw ; j++ )
        {
          // fazendo o bloco W
          fprintf(ifpa,"%c",0);
        }
        // gera as colunas do bloco A | A + Ia
        for( j=0; j < ia ; j++)
        {
          //printf("%d",0);
          fprintf(ifpa,"%c",255);
        }
        // printf("\n");
      }
    }

    for( i=0; i< ib; i++ ) // gerando as linhas do bloco B
    {
      // gerando as colunas  pares
      for( col=0, iw=I->W, ia=I->A; col< I->nh; col++, iw = iw + I->Iw, ia = ia + I->Ia )
      {
        // gera as colunas do bloco A | A + Ia
        for( j=0; j < iw ; j++)
        {
          fprintf(ifpa,"%c",255);
        }

        // geras as colunas do bloco w   |    w + Iw
        for( j=0; j < ia ; j++ )
        {
          // fazendo o bloco W
          fprintf(ifpa,"%c",0);
        }
      }
    }
  }
}

/****************************
 *
 *
 ****************************/
void geraLinha(char *nome, int lin, int col, int low, int high )
{
  int i, j, inc, x ;
  char pixel;
  printf("P5\n");
  fprintf(ifpa,"P5\n");
  printf("# quandrado gerado pelo geraPadr�o\n");
  fprintf(ifpa,"# quandrado gerado pelo geraPadr�o\n");
  printf("%d %d\n", col, lin );
  fprintf(ifpa,"%d %d\n", col, lin );
  printf("%d\n", high);
  fprintf(ifpa,"%d\n", high);
  inc = lin/high;
  if(inc == 0)
    inc = 1;
  for( i=0, pixel = low,x=0; i < lin; i++, x++ ) // gera as linhas
  {
    if(x == inc)
    {
      pixel++;
      x = 0;
      if(pixel > high)
        pixel = high;
    }

    for(j=0 ; j< col  ; j++ )
    {
      fprintf(ifpa,"%c",pixel);
    }
  }
}

/****************************
 *
 *
 ****************************/
void geraColuna(char *nome, int lin, int col, int low, int high )
{
  int i, j, inc,x ;
  char pixel;
  //ifpa = abreArq(nome);
  printf("P5\n");
  fprintf(ifpa,"P5\n");
  printf("# quandrado gerado pelo geraPadr�o\n");
  fprintf(ifpa,"# quandrado gerado pelo geraPadr�o\n");
  printf("%d %d\n", col, lin );
  fprintf(ifpa,"%d %d\n", col, lin );
  printf("%d\n", high);
  fprintf(ifpa,"%d\n", high);
  inc = col/high;
  if(inc == 0)
    inc = 1;
  for( i=0; i < lin; i++ ) // gera as linhas
  {
    for(j=0,x=0, pixel = low ; j< col  ; j++,x++)
    {
      if(x == inc)
      {
        pixel++;
        x = 0;
        if(pixel > high)
          pixel = high;
      }
      fprintf(ifpa,"%c",pixel);
    }
  }
}

/****************************
 *
 *
 ****************************/
void geraRand(char *nome, int lin, int col, int low, int high )
{
  int i, j ;
  char pixel;
  srand(time(NULL));
  printf("P5\n");
  fprintf(ifpa,"P5\n");
  printf("# quandrado gerado pelo geraPadr�o Rand\n");
  fprintf(ifpa,"# quandrado gerado pelo geraPadr�o\n");
  printf("%d %d\n", col, lin );
  fprintf(ifpa,"%d %d\n", col, lin );
  printf("%d\n", high);
  fprintf(ifpa,"%d\n", high);

  for( i=0; i < lin; i++ ) // gera as linhas
  {
    for(j=0 ; j< col  ; j++ )
    {
      pixel = rand()%high;
      fprintf(ifpa,"%c",pixel);
    }
  }
}


/****************************
 *
 *
 ****************************/
void geraRandL(char *nome, int lin, int col, int low, int high )
{
  int i, j;
  char pixel;
  srand(time(NULL));
  printf("P5\n");
  fprintf(ifpa,"P5\n");
  printf("# quandrado gerado pelo geraPadr�o RandL\n");
  fprintf(ifpa,"# quandrado gerado pelo geraPadr�o\n");
  printf("%d %d\n", col, lin );
  fprintf(ifpa,"%d %d\n", col, lin );
  printf("%d\n", high);
  fprintf(ifpa,"%d\n", high);
  for( i=0; i < lin; i++ ) // gera as linhas
  {
    pixel = rand()%high;
    for(j=0 ; j< col  ; j++ )
    {
      fprintf(ifpa,"%c",pixel);
    }
  }
}

/****************************
 *
 *
 ****************************/
void geraRandC(char *nome, int lin, int col, int low, int high )
{
  int i, j ;
  char *pixel;
  srand(time(NULL));
  //ifpa = abreArq(nome);
  printf("P5\n");
  fprintf(ifpa,"P5\n");
  printf("# quandrado gerado pelo geraPadr�o RandC\n");
  fprintf(ifpa,"# quandrado gerado pelo geraPadr�o\n");
  printf("%d %d\n", col, lin );
  fprintf(ifpa,"%d %d\n", col, lin );
  printf("%d\n", high);
  fprintf(ifpa,"%d\n", high);
  pixel = (char*)malloc(col * sizeof(char));
  for(j=0 ; j< col  ; j++ )
    pixel[j] = rand()%high;

  for( i=0; i < lin; i++ ) // gera as linhas
  {

    for(j=0 ; j< col  ; j++ )
    {
      fprintf(ifpa,"%c",pixel[j]);
    }
  }
  free(pixel);
}
